<?php
namespace Test\Php;

use Test\Php\CodingStandard\CodeMessDetector;
use Test\Php\CodingStandard\CodeSniffer;
use Test\Php\CodingStandard\CodeSnifferWrapper;
use PHPMD\TextUI\Command;
use PHPUnit_Framework_TestCase;

/**
 * Set of tests for static code analysis, e.g. code style, code complexity, copy paste detecting, etc.
 */
class CodingStandardTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var string
     */
    protected static $reportDir = '';

    /**
     * @var string
     */
    protected static $pathToSource = '';

    /**
     * Setup basics for all tests
     *
     * @return void
     */
    public static function setUpBeforeClass()
    {
        self::$pathToSource = BP;
        self::$reportDir = self::$pathToSource . '/dev/test/static/report';
        if (false === is_dir(self::$reportDir)) {
            mkdir(self::$reportDir);
        }
    }

    /**
     * get path to phpcs folder
     *
     * @return string
     */
    private function getPhpcsDir()
    {
        return realpath(self::$pathToSource . '/dev/CodingStandard/phpcs');
    }

    /**
     * get path to phpmd folder
     *
     * @return string
     */
    private function getPhpmdDir()
    {
        return realpath(self::$pathToSource . '/dev/CodingStandard/phpmd');
    }

    /**
     * Returns base folder for suite scope
     *
     * @return string
     */
    private static function getBaseFilesFolder()
    {
        return __DIR__;
    }

    /**
     * Returns base directory for whitelisted files
     *
     * @return string
     */
    private static function getChangedFilesBaseDir()
    {
        return __DIR__ . '';
    }

    /**
     * Returns whitelist based on blacklist and git changed files
     *
     * @param array $fileTypes
     * @param string $changedFilesBaseDir
     * @param string $baseFilesFolder
     * @return array
     */
    public static function getWhitelist($fileTypes = ['php'], $changedFilesBaseDir = '', $baseFilesFolder = '')
    {
        $globPatternsFolder = self::getBaseFilesFolder();
        if ('' !== $baseFilesFolder) {
            $globPatternsFolder = $baseFilesFolder;
        }
        $directoriesToCheck = array();
        $changedFiles = [];
        $globFilesListPattern = ($changedFilesBaseDir ?: self::getChangedFilesBaseDir()) . '/_files/changed_files*';
        foreach (glob($globFilesListPattern) as $listFile) {
            $changedFiles = array_merge($changedFiles, file($listFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));
        }
        array_walk(
            $changedFiles,
            function (&$file) {
                $file = BP . '/' . $file;
            }
        );
        #return $changedFiles;
        $changedFiles = array_filter(
            $changedFiles,
            function ($path) use ($directoriesToCheck, $fileTypes) {
                if (false === file_exists($path)) {
                    return false;
                }
                return true;
                $path = realpath($path);
                foreach ($directoriesToCheck as $directory) {
                    $directory = realpath($directory);
                    if (strpos($path, $directory) === 0) {
                        if (!empty($fileTypes)) {
                            return in_array(pathinfo($path, PATHINFO_EXTENSION), $fileTypes);
                        }
                        return true;
                    }
                }
                return false;
            }
        );
        return $changedFiles;
    }

    /**
     * Run the magento specific coding standards on the code
     *
     * @return void
     */
    public function testCodeStyle()
    {
        $reportFile = self::$reportDir . '/phpcs_report.txt';
        $wrapper = new CodeSnifferWrapper();
        $codeSniffer = new CodeSniffer(self::getPhpcsDir(), $reportFile, $wrapper);
        if (!$codeSniffer->canRun()) {
            $this->markTestSkipped('PHP Code Sniffer is not installed.');
        }
        $codeSniffer->setExtensions(['php', 'phtml']);
        $result = $codeSniffer->run(self::getWhitelist(['php', 'phtml']));
        $output = "";
        if (file_exists($reportFile)) {
            $output = file_get_contents($reportFile);
        }
        $this->assertEquals(
            0,
            $result,
            "PHP Code Sniffer has found {$result} error(s): " . PHP_EOL . $output
        );
    }

    /**
     * Run mess detector on code
     *
     * @return void
     */
    public function testCodeMess()
    {
        $reportFile = self::$reportDir . '/phpmd_report.txt';
        $codeMessDetector = new CodeMessDetector(self::getPhpmdDir() . '/ruleset.xml', $reportFile);

        if (!$codeMessDetector->canRun()) {
            $this->markTestSkipped('PHP Mess Detector is not available.');
        }

        $result = $codeMessDetector->run(self::getWhitelist(['php']));

        $output = "";
        if (true === file_exists($reportFile)) {
            $output = file_get_contents($reportFile);
        }

        $this->assertEquals(
            Command::EXIT_SUCCESS,
            $result,
            "PHP Code Mess has found error(s):" . PHP_EOL . $output
        );

        // delete empty reports
        if (true === file_exists($reportFile)) {
            unlink($reportFile);
        }
    }
}
