<?php

/**
 * PHP Code Mess tool wrapper
 */
namespace Test\Php\CodingStandard;

class CodeSnifferWrapper extends \PHP_CodeSniffer_CLI
{
    /**
     * Emulate console arguments
     *
     * @param $values
     * @return \Test\Php\CodingStandard\CodeSniffer
     */
    public function setValues($values)
    {
        $this->values = $values;
        return $this;
    }

    /**
     * Return the current version of php code sniffer
     *
     * @return string
     */
    public function version()
    {
        $version = '0.0.0';
        if (defined('\PHP_CodeSniffer::VERSION')) {
            $phpcs = new \PHP_CodeSniffer();
            $version = $phpcs::VERSION;
        }
        return $version;
    }
}
